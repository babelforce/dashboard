Dashboard-Collection

Use this URL to load your own JSON config file for your own dashboard:

https://apps.babelforce.com/dashboard/#?dashboard=<your URL>

Example:
https://apps.babelforce.com/dashboard/#?dashboard=https:%2F%2Fapps.babelforce.com%2Fdashboard%2Fr%2Fdemo%2Fdemo-sales